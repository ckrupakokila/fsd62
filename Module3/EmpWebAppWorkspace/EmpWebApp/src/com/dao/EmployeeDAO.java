package com.dao;

import java.sql.*;
import com.db.*;
import com.model.*;

public class EmployeeDAO {
	
	public Employee getEmployee(String emailId,String password){
		
		Connection con = GetConnection.getConnection();
		ResultSet rs=null;
		String query="select * from employee where emailid=? and password=?";
		try{
			
		PreparedStatement ps=con.prepareStatement(query);
		ps.setString(1, emailId);
		ps.setString(2, password);
		rs=ps.executeQuery();
		Employee emp=new Employee();
//		if ennpId--emoName then it is called as the she is no	if(rs.next()){
			emp.setId(rs.getInt(1));
			emp.setEmpName(rs.getString(2));
			emp.setEmailId(rs.getString(3));
			emp.setPassword(rs.getString(4));
			emp.setSalary(rs.getDouble(5));
			return emp;
		}
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return null;
	}
}