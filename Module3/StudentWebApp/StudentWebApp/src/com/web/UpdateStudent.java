package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.StudentDao;
import com.dto.Student;

@WebServlet("/UpdateStudent")
public class UpdateStudent extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		int studentId = Integer.parseInt(request.getParameter("studentId"));
		String studentName = request.getParameter("studentName");
		String gender = request.getParameter("gender");
		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");
		
		Student std = new Student(studentId, studentName, gender, emailId, password);
		
		StudentDao stdDao = new StudentDao();
		int result = stdDao.updateStudent(std);
		
		if (result > 0) {
			request.getRequestDispatcher("GetAllStudents").forward(request, response);
		} else {
			request.getRequestDispatcher("AdminHomePage").include(request, response);
			out.println("<br/>");
			out.println("<center>");
			out.println("<h3 style='color:red;'>Failed to Update the Employee Record!!!</h3>");
			out.println("</center>");
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}