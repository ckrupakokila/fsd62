package com.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.model.User;
import com.repository.UserRepository;
import com.util.PasswordUtil;

public class UserService {
	@Autowired
    private UserRepository userRepository;

    public User createUser(String email, String password) {
        User user = new User();
        user.setEmail(email);
        user.setPassword(PasswordUtil.encrypt(password)); // Encrypting password before storing
        return userRepository.save(user);
    }

    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }
}
