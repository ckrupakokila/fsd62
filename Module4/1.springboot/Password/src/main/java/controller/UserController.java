package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.model.User;
import com.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {
	 @Autowired
	    private UserService userService;

	    @PostMapping
	    public User createUser(@RequestParam String email, @RequestParam String password) {
	        return userService.createUser(email, password);
	    }

	    @DeleteMapping("/{id}")
	    public void deleteUser(@PathVariable Long id) {
	        userService.deleteUser(id);
	    }
}
