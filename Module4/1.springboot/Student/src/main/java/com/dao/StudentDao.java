package com.dao;

import java.util.List;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Student;

@Service
public class StudentDao {
	@Autowired
	StudentRepository stdRepo;

	public Student stdLogin(String emailId, String plaintextPassword) {
		Student student = stdRepo.findByEmailId(emailId);
		if (student != null) {
			String storedHashedPassword = student.getPassword();
			if (BCrypt.checkpw(plaintextPassword, storedHashedPassword)) {
				return student;
			}
		}
		return null;
	}

	//	public Student stdLogin(String emailId, String password) {
	//		return stdRepo.stdLogin(emailId, password);
	//	}

	public List<Student>getAllStudents() {
		return stdRepo.findAll();
	}

	public Student getStudentById(int stdId) {
		return stdRepo.findById(stdId).orElse(null);
	}

	public List<Student> getStudentByName(String stdName) {
		return stdRepo.findByName(stdName);
	}

	public Student addStudent(Student std) {
		return stdRepo.save(std);
	}

	public Student updateStudent(Student std) {
		return stdRepo.save(std);
	}

	public void deleteStudentById(int stdId) {
		stdRepo.deleteById(stdId);
	}
}