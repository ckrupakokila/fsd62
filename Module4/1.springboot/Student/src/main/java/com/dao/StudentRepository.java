package com.dao;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {
	Student findByEmailId(String emailId);
   

	@Query("from Student s where s.stdName = :name")
	List<Student> findByName(@Param("name") String stdName);

//	@Query("from Student s where s.emailId = :emailId and s.password = :password")
//	Student stdLogin(@Param("emailId") String emailId, @Param("password") String password);
}