import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { Test2Component } from './test2/test2.component';
import { ShowemployeesComponent } from './showemployees/showemployees.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HeaderComponent } from './header/header.component';
import { LogoutComponent } from './logout/logout.component';
import { ShowempbyidComponent } from './showempbyid/showempbyid.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ProductComponent } from './product/product.component';
import { CartComponent } from './cart/cart.component';
import { ExpPipe } from './exp.pipe';
import { GenderPipe } from './gender.pipe';


@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    Test2Component,
    ShowemployeesComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    LogoutComponent,
    ShowempbyidComponent,
    ProductComponent,
    CartComponent,
    ExpPipe,
    GenderPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RouterModule     
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
