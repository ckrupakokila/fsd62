import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmpService {

  isUserLoggedIn: boolean;

  //Dependency Injection for HttpClient
  constructor(private http: HttpClient) {
    this.isUserLoggedIn = false;
  }

  getAllContries() {
    return this.http.get('https://restcountries.com/v3.1/all');
  }

  
  setUserLoggedIn() {
    this.isUserLoggedIn = true;
  }
  setUserLoggedOut() {
    this.isUserLoggedIn = false;
  }
  getLoginStatus(): boolean {
    return this.isUserLoggedIn;
  }
  getAllEmployees() {
    return this.http.get('http://localhost:8085/getAllEmployees');
}
getAllDepartments() {
  return this.http.get('http://localhost:8085/getAllDepartments');
}
registerEmployee(employee: any) {
  return this.http.post('http://localhost:8085/addEmployee', employee);
}

employeeLogin(emailId: any, password: any) {
  return this.http.get('http://localhost:8085/empLogin/' + emailId + "/" + password).toPromise();
}

getEmployeeById(empId: any) {
  return this.http.get('http://localhost:8085/getEmployeeById/{id}' + empId).toPromise();
}
deleteEmployeeById(empId: any) {
  return this.http.delete('http://localhost:8085/deleteEmployeeById/' + empId).toPromise();
}getAllProducts(){
  return this.http.get('http://localhost:8085/getAllProducts');

}


}